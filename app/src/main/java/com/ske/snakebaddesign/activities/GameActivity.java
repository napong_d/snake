package com.ske.snakebaddesign.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ske.snakebaddesign.R;
import com.ske.snakebaddesign.guis.BoardView;
import com.ske.snakebaddesign.models.DieCup;
import com.ske.snakebaddesign.models.Game;
import com.ske.snakebaddesign.models.Piece;
import com.ske.snakebaddesign.models.Player;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;

public class GameActivity extends AppCompatActivity implements Observer {

//    private int boardSize;
//    private int p1Position;
//    private int p2Position;
//    private int turn;


    private BoardView boardView;
    private Button buttonTakeTurn;
    private Button buttonRestart;
    private TextView textPlayerTurn;
    private Game game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        initComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void initComponents() {
        game = new Game();
        game.addObserver(this);

        boardView = (BoardView) findViewById(R.id.board_view);
        boardView.setBoardSize(5);
        buttonTakeTurn = (Button) findViewById(R.id.button_take_turn);
        buttonTakeTurn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeTurn();
            }
        });
        buttonRestart = (Button) findViewById(R.id.button_restart);
        buttonRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetGame();
            }
        });
        textPlayerTurn = (TextView) findViewById(R.id.text_player_turn);
    }

    private void resetGame() {
        game.setTurn(0);
        game.getPlayer1().setPosition(0);
        game.getPlayer2().setPosition(0);
        game.setBoardSize(5);
        boardView.setBoardSize(5);
        boardView.setP1Position(0);
        boardView.setP2Position(0);
    }

    private void takeTurn() {
        final int value = game.getDieCup().roll();
        String title = "You rolled a die";
        String msg = "You got " + value;
        OnClickListener listener = new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                game.rolling(value);
                dialog.dismiss();
            }
        };
        displayDialog(title, msg, listener);
    }
//
//    private void moveCurrentPiece(int value) {
//        if (turn % 2 == 0) {
//            p1Position = adjustPosition(p1Position, value);
//            boardView.setP1Position(p1Position);
//            textPlayerTurn.setText("Player 2's Turn");
//        } else {
//            p2Position = adjustPosition(p2Position, value);
//            boardView.setP2Position(p2Position);
//            textPlayerTurn.setText("Player 1's Turn");
//        }
//        checkWin();
//        turn++;
//    }

//
    private void checkWin() {
        String title = "Game Over";
        String msg = game.getGameStatus();
        OnClickListener listener = new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                resetGame();
                dialog.dismiss();
            }
        };
        if(msg.equals("")){
            return;
        }
        displayDialog(title, msg, listener);
    }

    private void displayDialog(String title, String message, DialogInterface.OnClickListener listener) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", listener);
        alertDialog.show();
    }

    @Override
    public void update(Observable observable, Object data) {
        Player player = (Player) data;
        if(player.getNumplayer() == 0){
            boardView.setP1Position(player.getPosition());

        }
        else if(player.getNumplayer() == 1){
            boardView.setP2Position(player.getPosition());

        }
        checkWin();
        boardView.postInvalidate();
    }
}
