package com.ske.snakebaddesign.models;

/**
 * Created by Mr_Ja on 16/3/2559.
 */
public class DieCup {
    private Die die;

    public DieCup(){
        die = new Die();
    }

    public int roll(){
        return die.roll();
    }

}
