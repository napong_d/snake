package com.ske.snakebaddesign.models;

import java.util.Observable;

/**
 * Created by Mr_Ja on 16/3/2559.
 */
public class Game extends Observable {

    private DieCup diecup;
    private Player player1, player2;
    private int turn;
    private int boardSize;

    public Game() {
        player1 = new Player(0);
        player2 = new Player(1);
        diecup = new DieCup();
        boardSize = 5;
        turn = 0;
    }

    public void rolling(int face) {

        if (turn % 2 == 0) {
            if(player1.getPosition() + face > (boardSize * boardSize - 1)){
                player1.setPosition(adjustPosition(player1.getPosition(), 0));
            }
            else {
                player1.setPosition(adjustPosition(player1.getPosition(), face));
            }

            setChanged();
            notifyObservers(player1);
        } else {
            if(player2.getPosition() + face > (boardSize * boardSize - 1)){
                player2.setPosition(adjustPosition(player2.getPosition(), 0));
            }
            else{
                player2.setPosition(adjustPosition(player2.getPosition(), face));
            }

            setChanged();
            notifyObservers(player2);
        }

        turn++;

    }

    public int adjustPosition(int current, int distance) {
        current = current + distance;
        int maxSquare = boardSize * boardSize - 1;
        if (current > maxSquare) {
            current = maxSquare - (current - maxSquare);
        }
        return current;
    }

    public DieCup getDieCup() {
        return this.diecup;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setBoardSize(int size) {
        this.boardSize = size;
    }

    public String getGameStatus() {

        if (player1.getPosition() == boardSize * boardSize - 1) {
            return "Player 1 won!";
        } else if (player2.getPosition() == boardSize * boardSize - 1) {
            return "Player 2 won!";
        } else {
            return "";
        }

    }

}

