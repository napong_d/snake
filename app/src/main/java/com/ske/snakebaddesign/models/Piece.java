package com.ske.snakebaddesign.models;

/**
 * Created by Mr_Ja on 16/3/2559.
 */
public class Piece {
    private int position;

    public Piece(int position){
        this.position = position;
    }

    public int getPosition(){
        return this.position;
    }

    public void setPosition(int position){
        this.position = position;
    }

}
