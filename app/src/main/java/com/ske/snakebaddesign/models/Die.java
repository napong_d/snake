package com.ske.snakebaddesign.models;

import java.util.Random;

/**
 * Created by Mr_Ja on 16/3/2559.
 */
public class Die {
    public Die() {

    }

    public int roll(){
        int random = 0;
        random = new Random().nextInt(6) + 1;
        return random;
    }
}
