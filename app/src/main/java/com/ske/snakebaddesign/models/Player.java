package com.ske.snakebaddesign.models;

/**
 * Created by Mr_Ja on 16/3/2559.
 */
public class Player {
    private int numplayer;
    private Piece piece;

    public Player(int numplayer) {
        piece = new Piece(0);
        this.numplayer = numplayer;
    }

    public int getPosition(){
        return piece.getPosition();
    }

    public void setPosition(int position){
        piece.setPosition(position);
    }

    public int getNumplayer() {
        return numplayer;
    }
}
